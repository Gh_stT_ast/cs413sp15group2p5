package com.oreilly.demo.android.pa.uidemo.view;

import android.content.Context;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.oreilly.demo.android.pa.uidemo.model.Monster;
import com.oreilly.demo.android.pa.uidemo.model.Monsters;
import com.oreilly.demo.android.pa.uidemo.model.Grid;
import com.oreilly.demo.android.pa.uidemo.model.Cell;

/**
 * The main view for the application
 *
 * @author <a href="mailto:android@callmeike.net">Blake Meike</a>
 */
public class MonsterView extends View {

    /**
     * The monsters model
     */
    private volatile Monsters Monsters;
    /**
     * the grid model
     */
    private volatile Grid Grid;
    /**
     * a string used for logging
     */
    private final String TAG = "MonsterView";
    /**
     * our paint object
     */
    private Paint paint;

    /**
     * @param context the rest of the application
     */
    public MonsterView(Context context) {
        super(context);
        setFocusableInTouchMode(true);
    }

    /**
     * @param context the rest of the application
     * @param attrs the android attributes
     */
    public MonsterView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFocusableInTouchMode(true);
    }

    /**
     * @param context the rest of the application
     * @param attrs the android attributes
     * @param defStyle the androids defined style
     */
    public MonsterView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setFocusableInTouchMode(true);
    }

    /**
     * sets the monsters model instance var
     * @param Monsters the monsters model
     */
    public void setMonsters(Monsters Monsters) { this.Monsters = Monsters; }

    /**
     * sets the grid model instance var
     * @param Grid the grid model
     */
    public void setGrid(Grid Grid) { this.Grid = Grid; }

    /**
     * Redraws the canvas every time the view is notified of an update.
     * @see android.view.View#onDraw(android.graphics.Canvas)
     */
    @Override protected void onDraw(Canvas canvas) {
        paint = new Paint();
        paint.setStyle(Style.STROKE);
        paint.setColor(Color.GREEN);
        canvas.drawRect(0, 0, getWidth() - 1, getHeight() -1, paint);

        //draw the grid
//        Log.d( TAG, "Drawing Grid");
        if( null == Grid ) {
            Log.d(TAG, "Failed to draw Grid. Grid is null.");
            return;
        }
        paint.setStyle(Style.STROKE);
        paint.setColor(Color.GRAY);
        for( Cell c : Grid.getCells() ) {
            float left = c.getX();
            float top = c.getY();
            float right = c.getX() + (int) Grid.getCellDiameter();
            float bottom = c.getY() + (int) Grid.getCellDiameter();
            canvas.drawRect(
                left,
                top,
                right,
                bottom,
                paint
            );

            canvas.drawText(
                ((Integer) c.getId()).toString(),
                 left,
                 bottom,
                paint
            );
        }

        //draw the monsters
//        Log.d( TAG, "Drawing Monsters");
        if (null == Monsters || Monsters.getSize() == 0 ) {
            Log.d(TAG, "Failed to draw Monsters. Monsters is null.");
            return;
        }
        paint.setStyle(Style.FILL);
        for (Monster Monster : Monsters.getMonsters()) {
            paint.setColor(Monster.getColor());

            //make sure this monster has a home!
            int cellId = Monster.getCellId();
            if( cellId < 0 )
                break;

            Cell c = Grid.getCell( Monster.getCellId() );
//            Log.d( TAG, "Drawing Monster ID " + Monster.getId() + " in cell " + c.getId() );
            float left = c.getX();
            float top = c.getY();
            float right = c.getX() + (int) Grid.getCellDiameter();
            float bottom = c.getY() + (int) Grid.getCellDiameter();
            canvas.drawRect(
                left,
                top,
                right,
                bottom,
                paint
            );
        }
    }
}
