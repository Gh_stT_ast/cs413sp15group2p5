package com.oreilly.demo.android.pa.uidemo.model;

import android.graphics.Color;


/**
 * Shared constants throughout the project
 */
public class Constants {

    /**
     * the vulnerable state of a monster
     */
    public static final int VULNERABLE = Color.YELLOW;
    /**
     * the invulnerable state of a monster
     */
    public static final int PROTECTED = Color.GREEN;


    /**
     * the base number of monsters to display
     */
    public static final int BASE_MONSTERS_COUNT = 2; //min starting
    /**
     * extra monsters per level
     */
    public static final int MONSTER_LVL_MULTIPLIER = 3; //number of monsters to add per level


    /**
     * the maximum level
     */
    public static final int HIGHEST_LVL = 10; //maximum number of level (must leave enough squares for monsters!)
    /**
     * the time allowed per level
     */
    public static final int LVL_TIME_ALLOWED = 30;

    /**
     * base percentage that a monster will become vulnerable onTick()
     */
    public static final int BASE_VULN_PERCENT = 70;
    /**
     * multiplier per level that affects BASE_VULN_PERCENT
     */
    public static final double MONSTER_VULN_LVL_MULTIPLER = 0.9;

}
