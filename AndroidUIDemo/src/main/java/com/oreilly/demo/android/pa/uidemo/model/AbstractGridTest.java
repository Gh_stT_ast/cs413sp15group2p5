package com.oreilly.demo.android.pa.uidemo.model;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runners.Parameterized;

import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Created by claudiusouca on 4/28/15.
 */
public class AbstractGridTest {

    protected Grid grid;

    /**
     * Setter for dependency injection. Usually invoked by concrete testcase
     * subclass.
     *
     *
     */

    //Tests that grid is starts out empty
    @Test
    public void testPreconditions() {
        // tests that grid starts
        assertEquals(grid.initialized(), false);
        assertNull(grid.getCellCount());
        assertNull(grid.getCellDiameter());
        assertNull(grid.getCellCols());

    }


    // Sets values for Grid
    @Before
    public void setUp() throws Exception {
        grid.initialized();
        grid.setDimensions(150, 150);

    }

    //Tests that the Grid was set
    @Test
    public void TestInitialization() throws Exception {

        assertEquals(grid.initialized(), true);
        assertNotNull(grid.getCellCount());
        assertNotNull(grid.getCellDiameter());
        assertNotNull(grid.getCellCols());
    }

    //Test that the Grid is taken down
    @After
    public void tearDown() throws Exception {

        assertEquals(grid.initialized(), false);
        assertNull(grid.getCellCount());
        assertNull(grid.getCellDiameter());
        assertNull(grid.getCellCols());

    }
}

