package com.oreilly.demo.android.pa.uidemo.model;

import android.os.AsyncTask;
import android.util.Log;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

/** A monster: the coordinates, color and size. */
public final class Monster extends AsyncTask<String, Void, Void> {
    /**
     * the monster color and monster ID
     */
    private int color, monsterId;
    /**
     * a random instance used for generating movement
     */
    private final Random rand = new Random();
    /**
     * the cell this monster belongs to, null until assigned
     */
    private Cell cell;
    /**
     * a boolean if the async task has started
     */
    private Boolean started = false;
    /**
     * string used for logging/debugging
     */
    private final String TAG = "MONSTER";
    /**
     * a timer used in background tasks to help move.
     * doesn't use the same timer as onTick() as we want the monsters to move at different times
     * from each other
     */
    private Timer timer;
    /**
     * a reference to the Monsters Model
     */
    private final Monsters monsters;

    /**
     * constructor, sets up the instance vars
     * @param color the color
     * @param id the monster id
     * @param monsters the reference to monsters model
     */
    public Monster( int color, int id, Monsters monsters) {
        this.color = color;
        this.monsterId = id;
        this.monsters = monsters;
    }

    /** @return the color. */
    public int getColor() { return color; }

    /**
     *  sets the color
     *  @param c the integer value of the color
     */
    public void setColor( int c ) { color = c; }

    /** @return the monster id */
    public int getId() { return monsterId; }

    /**
     * sets the id of the cell the monster is residing in
     * @param cell the cell object
     */
    public void setCell( Cell cell ) {
        this.cell = cell;
    }

    /**
     * @return the cell this monster is currently residing in
     */
    public Cell getCell() {
        return cell;
    }

    /**
     * @return the id this monster is currently residing in, -1 if not yet assigned
     */
    public int getCellId() {
        return ( cell == null ) ? -1 : cell.getId();
    }

    @Override
    /**
     * our async task. Handles the moving
     */
    protected Void doInBackground(String... params) {
        Log.d(TAG, "In process of executing monster " + this.getId() + " background task");

        //instantiate the timer
        timer = new Timer();

        //set up our delays using the following formula:
        //rand.nextInt(upperbound-lowerbound) + lowerbound;
        int initialDelay = rand.nextInt(1000 - 200 ) + 200;
        int periodicDelay = rand.nextInt(2500 - 800 ) + 800;

        //schedule the timer based on the delays
        timer.schedule(new TimerTask() {
            @Override public void run() {
                //move the monster and notify the view to redraw
                move();
                monsters.notifyListener();
            }
        }, initialDelay, periodicDelay);

        return null;
    }

    /**
     * moves the monster to a random neighbor cell
     */
    public void move() {
        //try to move only if cell has already been assigned
        if( getCellId() >= 0 ) {
//            Log.d(TAG, "Monster ID " + monsterId + " in cell " + cell.getId() + "...requesting move..." );
            setCell( cell.getRandomNeighbor() );
//          Log.d(TAG, "Monster ID " + monsterId + " moved to cell " + cell.getId() );
        }
    }

    /**
     * called when a monster is killed
     * makes the cell forget this monster
     */
    public void die() {
        timer.cancel(); //destroy the timer
        cell.monsterLeave(); //notify the cell that it's free
        setCell( null ); //make this monster forget about where it was so it isn't redrawn
    }

    /**
     * @return boolean if async task (moving) has started
     */
    public boolean started() {
        return started;
    }

    /**
     * starts the background task
     */
    public void start() {
        started = true;
        execute();
    }
}