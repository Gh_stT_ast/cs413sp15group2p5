package com.oreilly.demo.android.pa.uidemo.model.clock;

import android.widget.TextView;

import com.oreilly.demo.android.pa.uidemo.model.Monsters;

/**
 * The active model of the internal clock that periodically emits tick events.
 *
 * @author laufer
 */
public interface ClockModel extends OnTickSource {
    /**
     * Starts the clock timer
     */
	void start();

    /**
     * Cancels the clock timer
     */
	void stop();
}
