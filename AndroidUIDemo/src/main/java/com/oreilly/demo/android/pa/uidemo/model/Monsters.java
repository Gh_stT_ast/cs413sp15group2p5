package com.oreilly.demo.android.pa.uidemo.model;

import android.graphics.Color;
import android.util.Log;

import com.oreilly.demo.android.pa.uidemo.model.clock.OnTickListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;


/** A list of monsters. */
public class Monsters implements OnTickListener {
    /** MonsterChangeListener. Notifies the view when a change in monsters occurs */
    public interface MonstersChangeListener {
        /** @param monsters the monsters that changed. */
        void onMonstersChange(Monsters monsters);
    }
    /**
     * a connection to the view and controller used to notify change in monsters
     */
    private MonstersChangeListener monstersChangeListener;

    /** LevelChangeListener. */
    public interface LevelCompleteListener {
        /** @param level the new level. */
        void onLevelComplete(Integer level);
    }
    /**
     * a connection to the view and controller used to notify change in level
     */
    private LevelCompleteListener levelCompleteListener;

    /**
     * hashmap containing a list of monsters and their ids
     */
    private final HashMap<Integer, Monster> monsters = new HashMap();
    /**
     * a counter to determine monster ID upon generation
     */
    private int monsterIdCounter = 0;
    /**
     * the current level. Passed as object reference to the controller during level complete notifications
     * synced with the TouchMe (controller) level object
     */
    private Integer level;
    /**
     * a string for logging
     */
    private final String TAG = "Monsters";
    /**
     * random object used to help determine if monster goes vulnerable or not onTick
     */
    private final Random rand = new Random();
    /**
     * has furthered calculations applied on level change to determine percentage chance monster
     * goes vulnerable on tick
     */
    private int vulnPercent = Constants.BASE_VULN_PERCENT;
    /**
     * an internal timer used to keep track of how much time until game over
     */
    private int timeRemaining = Constants.LVL_TIME_ALLOWED;
    /**
     * keeps track of score throughout each game. Resets on game reset
     */
    private int score = 0;

    /** @param l set the change listener. */
    public void setMonstersChangeListener(MonstersChangeListener l) {
        monstersChangeListener = l;
    }
    /** @param l set the change listener. */
    public void setLevelCompleteListener(LevelCompleteListener l) {
        levelCompleteListener = l;
    }

    /** @return list of monsters. */
    public List<Monster> getMonsters() {
        return new ArrayList( monsters.values() );
    }

    /**
     * @param color monster color.
      */
    public void addMonster(int color) {
        monsters.put( monsterIdCounter, new Monster( color, monsterIdCounter, this ) );
        monsterIdCounter++;
        notifyListener();
    }

    /**
     * Kills a monster. Checks for a win condition
     * @param monsterId the monster id
     */
    public void killMonster( int monsterId ) {
        Log.d(TAG, "Monster " + monsterId + " has been killed!");
        //increase the score
        score++;
        //get the monster
        Monster m = monsters.get( monsterId );
        //kill the monster
        m.die();
        //remove this monster from our model
        monsters.remove(monsterId);
        //notify the view
        notifyListener();

        //check if the user won
        if( getSize() < 1 ) {
            //user wins!
            score += timeRemaining * 2; //2 points for each second remaining
            timeRemaining = 0;
            level++;
            notifyLevelListener();
        }

    }

    /** Remove all monsters. */
    public void killAllMonsters() {
        //kill the monster so the cell is freed also
        for( Monster m : getMonsters() )
            m.die();

        //ensure the list is cleared
        monsters.clear();

        //reset the score and time
        score = 0;
        timeRemaining = Constants.LVL_TIME_ALLOWED;

        //notify the view
        notifyListener();
    }

    /**
     * changes the level of monster difficulty
     * @param level number to set
     */
    public void changeLevel( Integer level ) {
        //update per level conditions
        this.level = level;
        this.timeRemaining = Constants.LVL_TIME_ALLOWED;
        calculateVulnerabilityPercentage();

        //add the appropriate amount of monsters
        while( getSize() < ( Constants.BASE_MONSTERS_COUNT + ( level * Constants.MONSTER_LVL_MULTIPLIER ) ) ) {
            addMonster( Color.GREEN );
        }
    }

    /**
     * Sets the vulnerability percentage based on the current level
     */
    public void calculateVulnerabilityPercentage() {
        double multiplier = 1;
        for( int i = 1; i <= level; i++ )
            multiplier *= Constants.MONSTER_VULN_LVL_MULTIPLER; //eg, level 2: 1 * .9 .9 = .81
        vulnPercent = (int) Math.ceil(vulnPercent * multiplier );
        Log.d(TAG, "multiplier: " + multiplier );
        Log.d(TAG, "Vulnerability Percent: " + vulnPercent);
    }

    /**
     * notifies the listener that a monster has been updated somehow
     * usually used to update the drawing
     */
    public void notifyListener() {
        if (null != monstersChangeListener) {
            monstersChangeListener.onMonstersChange(this);
        }
    }

    /**
     * notifies the listener that a monster has been updated somehow
     * usually used to update the drawing
     */
    private void notifyLevelListener() {
        if (null != levelCompleteListener) {
            levelCompleteListener.onLevelComplete(level);
        }
    }


    /**
     * @return the integer size of the monsters hashmap
     */
    public int getSize() {
        return monsters.size();
    }

    /**
     * Runs every time the default clock model timer loops.
     * iterates through the monsters and determines if they go vulnerable or invulnerable
     * starts the monster async task if it hasn't yet started
     */
    public void onTick() {

        for( Monster m: getMonsters() ) {
            int chance = rand.nextInt(100);
//          Log.d(TAG, "Random onTick value: " + chance );
            //chance a monster goes/stays vulnerable

            if( chance <= vulnPercent ) {
                //set it vulnerable
//              Log.d(TAG, "Executing monster " + m.getId() + " background task");/
                m.setColor( Constants.VULNERABLE );

            } else {
                //make it green
                m.setColor( Constants.PROTECTED );
            }

            //if the monster hasn't started executing its async task, do so now
            if( !m.started() ) {
                try {
                    m.start();
                } catch (IllegalStateException e ) {
                    //thread already started
                }
            }
        }

        //decrement our time remaining (never let it go below 0)
        if( timeRemaining > 0)
            timeRemaining--;

        //notify the view
        notifyListener();
    }

    /**
     * @return the number of seconds remaining until game over
     */
    public int getTimeRemaining() {
        return timeRemaining;
    }

    /**
     * @return the cumulative game score
     */
    public int getScore() {
        return score;
    }
}
