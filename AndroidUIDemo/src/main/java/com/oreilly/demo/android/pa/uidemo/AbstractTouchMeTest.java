package com.oreilly.demo.android.pa.uidemo;

import android.content.pm.ActivityInfo;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.Button;
import android.widget.TextView;

import junit.framework.TestCase;

import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Abstract GUI-level test superclass for several essential click-counter
 * scenarios.
 *
 */
@Ignore
public abstract class AbstractTouchMeTest {




    /**
     * Verifies that the activity under test can be launched.
     */
    @Test
    public void testActivityTestCaseSetUpProperly() {
        assertNotNull("activity should be launched successfully", getActivity());
    }

    // Tests that Text Views display the proper values
    @Test
    public void testActivity() {
        assertEquals(getLevel(), getViewLevel());
        assertEquals(getTime(), getViewTime());
        assertEquals(getScore(), getViewScore());
    }



    public abstract TouchMe getActivity();


    // Return the score value
    protected int getScore() {
        final TextView t = (TextView) getActivity().findViewById(R.id.scoreValue);
        return Integer.parseInt(t.getText().toString().trim());
    }

    // Get the TextView which holds the Level text
    protected int getLevel() {
        final TextView t = (TextView) getActivity().findViewById(R.id.levelValue);
        return Integer.parseInt(t.getText().toString());
    }
    // Get the TextView which holds the Time text
    protected int getTime() {
        final TextView t = (TextView) getActivity().findViewById(R.id.timeRemainingValue);
        return Integer.parseInt(t.getText().toString());
    }

    // Get the TextView which holds the Score text
    protected int getViewScore() {
        final TextView t = (TextView) getActivity().findViewById(R.id.scoreText);
        return Integer.parseInt(t.getText().toString().trim());
    }
    // Get the TextView which holds the Level text
    protected int getViewLevel() {
        final TextView t = (TextView) getActivity().findViewById(R.id.levelText);
        return Integer.parseInt(t.getText().toString());
    }
    // Get the TextView which holds the Time
    protected int getViewTime() {
        final TextView t = (TextView) getActivity().findViewById(R.id.timeRemainingText);
        return Integer.parseInt(t.getText().toString());
    }

    //Makes sure values don't change during rotation
    @Test
    public void testActivityScenarioRotation() {
        int levelBefore = getLevel();
        int scoreBefore = getScore();
        int timeBefore = getTime();
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        assertEquals(levelBefore, getLevel());
        assertEquals(scoreBefore, getScore());
        assertEquals(timeBefore, getTime());

    }



}
