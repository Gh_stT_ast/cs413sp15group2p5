package com.oreilly.demo.android.pa.uidemo.model;

import android.util.Log;

/**
 * Represents a single cell in the grid. Each cell can hold 0-1 Monster
 */
public class Cell {
    /**
     * the id, the left-most x-coordinate, the upper-most y-coordinate
     */
    private final int id, x, y;
    /**
     * the monster this cell holds. null when no monster
     */
    private Monster monster;
    /**
     * a tag for logging
     */
    private final String TAG = "Cell";
    /**
     * a reference to the parent grid
     */
    private final Grid grid;
    /**
     * An object representing a mutex (binary-semaphore like)
     */
    private Object mutex = new Object();

    /**
     * Constructor. Sets the instance vars
     * @param id the cell id (assigned by grid)
     * @param x the left-most x-coordinate
     * @param y the upper-most y-coordinate
     * @param grid a reference to the parent grid
     */
    public Cell( int id, int x, int y, Grid grid ) {
        this.id = id;
        this.x = x;
        this.y = y;
        this.monster = null;
        this.grid = grid;
    }

    /**
     * receives a touch event and attempts to kill the monster in this cell
     * @param monsters the monsters model (received from the controller TouchMe)
     */
    public void onTouch( Monsters monsters ) {
        Log.d( TAG, "Cell " + id + " acknowledging touch." );
        //is there a monster in this cell?
        if( monster == null ) {
            Log.d( TAG, "Cell " + id + " has no monster in it" );
            return;
        }

        //is the monster vulnerable?
        if( monster.getColor() == Constants.VULNERABLE ) {
            Log.d( TAG, "Cell " + id + " monster is vulnerable - sending kill command" );
            monsters.killMonster( monster.getId() );
        } else {
            Log.d( TAG, "Cell " + id + " monster is not vulnerable!" );
        }
    }

    /**
     * accepts a monster (assigns monster to this cel)
     * @param m the monster
     * @return true on success, false on failure (cell is occupied)
     */
    public boolean acceptMonster( Monster m ) {
//        Log.d(TAG, "Monster " + m.getId() + " trying for cell " + id );
        //is there a monster in this cell?
        if( this.monster != null ) {
//            Log.d( TAG, "This cell is occupied. Refusing monster.");
            return false;
        }

        //update this cell to reflect this monster, and the reverse with the monster
        this.monster = m;
        m.setCell( this );
//        Log.d( TAG, "This cell has accepted the monster." );
        return true;
    }

    /**
     * allows a monster to leave (by moving or death)
     * nullifies this.monster
     */
    public void monsterLeave() {
        this.monster = null;
    }

    /**
     * There are 9 possible neighbors, self included (edges "roll over");
     * see Grid getRandomNeighbor for full picture
     * @return random neighboring cell
     */
    public Cell getRandomNeighbor() {
        //get the new cell id
        int newCellId = grid.getRandomNeighbor( getId() );
        Boolean change = false;
        Cell newCell;
        //lock the neighbor cell
        synchronized ( grid.getCellMutex( newCellId ) ) {
            // Log.d( TAG, "Random neighbor: " + newCellId );

            //get the new cell
            newCell = grid.getCell( newCellId );

            try {
                change = newCell.acceptMonster(monster);
            } catch( NullPointerException e ) {
                //monster may have been killed during switch.. just ignore the change and it'll get
                //cleaned up during redraw
            }
            // Log.d( TAG, "Cell changing? " + change );
        }

        //return the new cell, or this cell if no change
        if( change ) {
            monsterLeave();
            return newCell;
        }

        return this;
    }

    /**
     * @return the cell id
     */
    public int getId() {
        return id;
    }

    /**
     * @return starting x coordinate
     */
    public int getX() { return x; }
    /**
     * @return starting y coordinate
     */
    public int getY() { return y; }

    /**
     * @return the lock object
     */
    public Object getMutex() {
        return mutex;
    }
}
