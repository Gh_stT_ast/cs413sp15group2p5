package com.oreilly.demo.android.pa.uidemo.model.clock;

/**
 * A listener for onTick events coming from the internal clock model.
 *
 * @author laufer
 */
public interface OnTickListener {

    /**
     * generic method called to reflect seconds passing from the timer
     * Subclasses define action being taken for each tick
     */
	public void onTick();
}
