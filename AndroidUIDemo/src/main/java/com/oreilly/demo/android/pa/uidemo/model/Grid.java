package com.oreilly.demo.android.pa.uidemo.model;

import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

/**
 * The Grid Model. Contains a grid of cells
 */
public class Grid {

    /**
     * cells: a hashmap of all the cells and cell id
     */
    private HashMap<Integer, Cell> cells = new HashMap();
    /**
     * the number of rows in the grid
     */
    private int gridRows;
    /**
     * the number of columns in the grid
     */
    private int gridCols;
    /**
     * the number of cells in the grid
     */
    private int cellCount;
    /**
     * the diameter of each cell (based on screen resolution)
     */
    private float cellDiameter;
    /**
     * a string used for logging
     */
    private String TAG = "GRID";
    /**
     * boolean to detect if grid has already been initialized (to prevent redundant operations)
     */
    private int gridInit = 0;
    /**
     * a reference to a Random which is used to auto assign monsters to cells
     */
    private final Random rand = new Random();

    /**
     *  Instantiates the grid based on view view port
     * @param viewWidth integer value of view Width in pixels
     * @param viewHeight integer value of the view height in px
     */
    public void setDimensions( int viewWidth, int viewHeight ) {

        //don't redo this
        if( initialized() )
            return;

        //calculate the number of cells
        //based on view viewport and cell dimension
        int minCellDiameter = 150;

        Log.d(TAG, "Instantiating the Grid");
        Log.d(TAG, "view Height: " + viewHeight );
        Log.d(TAG, "view Width: " + viewWidth );

        //should always return floor as int, but just to be safe
        int maxCellRows = (int) Math.floor( viewHeight /  minCellDiameter );
        int maxCellCols = (int) Math.floor( viewWidth / minCellDiameter );
        Log.d(TAG, "Max Cell Rows: " + maxCellRows );
        Log.d(TAG, "Max Cell Cols: " + maxCellCols );

        //calculate the cell diameter, ensure it is at least max size
        float cellWidthRows = viewHeight / maxCellRows;
        float cellWidthCols = viewWidth / maxCellCols;
        cellDiameter = Math.max(Math.min(cellWidthCols, cellWidthRows), minCellDiameter);
        Log.d(TAG, "Cell Width Rows: " + cellWidthRows );
        Log.d(TAG, "Cells Width Cols: " + cellWidthCols );
        Log.d(TAG, "Cell Diameter: " + cellDiameter );

        //now calculate the final number of rows and columns
        gridRows = (int) Math.floor( viewHeight / cellDiameter );
        gridCols = (int) Math.floor( viewWidth / cellDiameter );
        Log.d(TAG, "Grid Rows: " + gridRows );
        Log.d(TAG, "Grid Cols: " + gridCols );

        cellCount = gridRows * gridCols;
        Log.d(TAG, "Cell Count: " + cellCount );

        //eg: 320x480 view viewport
        //maxCellRows = 9
        //maxCellCols = 6
        //cellDiameter = 53.3
        //gridRows = 9
        //gridCols = 6
        //cellCount = 54

        //fill the hashmap by creating the cells
        populateGrid();

        //mark our grid as initialized
        gridInit = 1;
    }

    /**
     * @return boolean if the grid has been init or not
     */
    public boolean initialized() {
        return gridInit == 1;
    }

    /**
     * Fills the hashmap Cells with cellCount number of Cell
     */
    private void populateGrid() {
        int id = 0;
        int x = 0;
        int y = 0;

        //iterate through the columns
        for( int r = 1; r <= gridRows; r++ ) {
//            Log.d( TAG, "Populating row " + r );
            //iterate through the rows
            for( int c = 1; c <= gridCols; c++ ) {
//                Log.d( TAG, "Populating column " + c );
                cells.put( id, new Cell( id, x, y, this ) );
//                Log.d( TAG, "Cell ID: " + id + " value: " + cells.get(id).toString() );
                x += cellDiameter;
                id++;
            }

            x = 0;
            y += cellDiameter;
        }

        //should never occur, but let's log it if it does
        if( id != cellCount ) {
            Log.e( TAG, "Cell count does not match ID. The grid may not have been initialized properly!" );
        }
    }

    /**
     * returns the cell specified
     * @param cellId the id of the requested cell
     * @return the cell
     */
    public Cell getCell( int cellId ) {
        return cells.get( cellId );
    }

    /**
     * @return list of cells
     */
    public List<Cell> getCells() {
        return new ArrayList<Cell>(cells.values());
    }

    /**
     * returns the cell id/number based on x y coordinates where the user clicked
     */
    public int getCellId( int x, int y ) {
        Log.d( TAG, "Looking up cell id for (" + x + ", " + y + ")");
        int rowNum = (int) Math.ceil( y / cellDiameter );
        Log.d( TAG, "Row num: " + rowNum);
        int colNum = (int) Math.ceil( x / cellDiameter );
        Log.d( TAG, "Col num: " + colNum);

        return ( ( rowNum - 1 ) * gridCols ) + colNum - 1;
    }

    /**
     * receives a touch from the controller
     * forwards the touch to the specific cell
     * @param cellId the id of the cell to forward the toucht o
     * @param monsters the monsters model
     */
    public void touchCell( int cellId, Monsters monsters ) {
        Cell cell = cells.get(cellId);

        //allow them to click on the edges of the grid without crashing the app
        if( cell == null )
            return;

        //forward the touch from the controller to the cell model
        cell.onTouch(monsters);
    }

    /**
     * Accepts Monsters and assigns them to a random cell
     * @param monsters: the monsters
     */
    public void acceptMonsters( Monsters monsters ) {

        if( !initialized() ) {
            Log.e( TAG, "Grid is not yet initialized" );
            return;
        }

        //iterate through the monsters and assign them to a cell
        Boolean match;
        List<Monster> listMonsters = monsters.getMonsters();
        for( Monster m : listMonsters ) {
            do {
                int cellId = rand.nextInt( cellCount - 1 );
                Cell cell = cells.get( cellId );
//                Log.d( TAG, "Monster ID " + m.getId() + " is checking cell ID " + cellId + " (value " + cell.toString() + ")");
                match = cell.acceptMonster(m);
//                Log.d( TAG, "Monster ID " + m.getId() + " is now in cell ID " + m.getCellId() );
            } while( !match );
        }
    }

    /**
    * @return cell diameter
    */
    public float getCellDiameter() {
          return cellDiameter;
      }

    /**
     * @return the number of cells in the grid
     */
    public int getCellCount() {
        return cellCount;
    }

    /**
     * @return the integer of cols per row
     */
    public int getCellCols() {
        return gridCols;
    }

    /**
     * There are 9 possible neighbors, self included (edges "roll over");
     * @return a cell id from a random neighbor
     */
    public int getRandomNeighbor( int cellId ) {
        //get a val between 0-8
        int chance = rand.nextInt( 9 );

        /*
        Let n = number of cols per row
        let 0 = starting point
        To find our neighbor, we need to move according to the following formula (table)

                  [A]   [B]   [C]
           [1]   -n-1 | -n  | -n+1
                --------------------
           [2]    -1  |  0  |  +1
                --------------------
           [3]   n-1  |  n  |  n+1

        let 1A = 0, 1B = 1, 1C = 2
        let 2A = 3, 2B = 4, 2C = 5
        let 3A = 6, 3B = 7, 3C = 8
         */
        int[] moveFormula = new int[9];
        int n = getCellCols();
        moveFormula[0] = -n - 1; //1A
        moveFormula[1] = -n; //1B
        moveFormula[2] = -n + 1; //1C
        moveFormula[3] = -1; //2A
        moveFormula[4] = 0; //2B
        moveFormula[5] = 1; //2C
        moveFormula[6] = n - 1; //3A
        moveFormula[7] = n; //3B
        moveFormula[8] = n + 1; //3C
        int neighbor = cellId + moveFormula[ chance ];

        //now mod it by the number of grid cells so if we go over the count we start at the beginning
        neighbor = neighbor % getCellCount();
        if( neighbor < 0 )
            neighbor = getCellCount() + neighbor;
        return neighbor;
    }

    /**
     * @return the lock (mutex) from a specified cell (used to synchronized locking)
     */
    public Object getCellMutex( int cellId ) {
//        Log.d( TAG, "Fetching lock for cell " + cellId );
        return (getCell( cellId )).getMutex();
    }
}
