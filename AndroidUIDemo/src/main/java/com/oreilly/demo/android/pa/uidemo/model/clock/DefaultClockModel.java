package com.oreilly.demo.android.pa.uidemo.model.clock;

import android.util.Log;
import android.widget.TextView;

import com.oreilly.demo.android.pa.uidemo.R;
import com.oreilly.demo.android.pa.uidemo.model.Monsters;

import java.util.Timer;
import java.util.TimerTask;

/**
 * An implementation of the internal clock.
 *
 * @author laufer
 */
public class DefaultClockModel implements ClockModel {

	private final String TAG = "DefaultClock";

	private Timer timer;

	private OnTickListener listener;

	@Override
	public void setOnTickListener(final OnTickListener listener) {
		this.listener = listener;
	}

    /**
     * instantiates a new timer. Sends a tick event to the listener every second.
     */
	@Override
	public void start() {
		timer = new Timer();

		// The clock model runs onTick every 1000 milliseconds
		timer.schedule(new TimerTask() {
			@Override public void run() {
				// fire event
				listener.onTick();
			}
		}, /*initial delay*/ 1000, /*periodic delay*/ 1000);
	}

    /**
     * cancels the current timer. Timer is no longer available.
     * New timer must be created.
     */
	@Override
	public void stop() {
		timer.cancel();
	}
}