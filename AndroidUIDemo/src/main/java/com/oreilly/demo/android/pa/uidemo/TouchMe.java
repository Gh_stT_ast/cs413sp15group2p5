package com.oreilly.demo.android.pa.uidemo;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View.OnFocusChangeListener;
import android.widget.TextView;

import com.oreilly.demo.android.pa.uidemo.model.Constants;
import com.oreilly.demo.android.pa.uidemo.model.Grid;
import com.oreilly.demo.android.pa.uidemo.model.Monsters;
import com.oreilly.demo.android.pa.uidemo.model.clock.ClockModel;
import com.oreilly.demo.android.pa.uidemo.model.clock.DefaultClockModel;
import com.oreilly.demo.android.pa.uidemo.model.clock.OnTickListener;
import com.oreilly.demo.android.pa.uidemo.view.MonsterView;


/**
 * Android UI demo program
 */
public class TouchMe extends Activity implements OnTickListener {

    /**
     * @var String used for logging
     */
    private String TAG = "TOUCH";
    /**
     * @var the current game level. Will be synced with the monster model
     */
    Integer level = new Integer(1); //the user level
    /**
     * @var the clock model instance. used for onTick events
     */
    private ClockModel clockModel = new DefaultClockModel();

    /** Listen for taps. */
    private static final class TrackingTouchListener
        implements View.OnTouchListener
    {
        /**
         * @var the monsters model
         */
        private final Monsters mMonsters;
        /**
         * @var the grid model
         */
        private final Grid mGrid;
        /**
         * @var a string used for logging
         */
        private final String TAG = "TrackingTouchListener";

        /**
         * constructor. sets instance vars
         * @param mMonsters the monster model
         * @param mGrid the grid model
         */
        TrackingTouchListener(Monsters mMonsters, Grid mGrid) {
            this.mMonsters = mMonsters;
            this.mGrid = mGrid;
        }

        /**
         * receives and handles a touch event
         * @param v the view to update
         * @param e the event object
         */
        @Override public boolean onTouch(View v, MotionEvent e) {

            //we only care about where the finger was when they lifted it up
            int action = e.getAction();
            if( action == MotionEvent.ACTION_UP ) {
                float x = e.getX();
                float y = e.getY();
                //figure out the cell id based on the coordinates
                int cellId = mGrid.getCellId( (int) Math.ceil( x ), (int) Math.ceil( y ) );
                Log.d( TAG, "Touch received to (x, y): (" + x + ", " + y + ") -- Cell ID: " + cellId );
                //forward the touch from controller to view
                mGrid.touchCell( cellId, mMonsters );
            }
            return true;
        }
    }

    /**
     * @var the application model for monsters
     */
    final Monsters MonsterModel = new Monsters();
    /**
     * @var the application model for the grid
     */
    final Grid GridModel = new Grid();

    /**
     * @var the application view
     */
    MonsterView MonsterView;

    /** Called when the activity is first created. */
    @Override public void onCreate(Bundle state) {
        super.onCreate(state);

        // install the view
        setContentView(R.layout.main);

        //generate the monsters
        MonsterModel.changeLevel( level );

        // find the Monsters view
        MonsterView = (MonsterView) findViewById(R.id.Monsters);
        MonsterView.setMonsters(MonsterModel);
        MonsterView.setGrid(GridModel);

        //set up the listeners for menu or screen tapping
        MonsterView.setOnCreateContextMenuListener(this);
        MonsterView.setOnTouchListener(new TrackingTouchListener(MonsterModel, GridModel));

        //once the view has focus, we can set the grid dimensions and assign the monsters
        MonsterView.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus && !GridModel.initialized() ) {
                    GridModel.setDimensions( v.getWidth(), v.getHeight() );
                    GridModel.acceptMonsters( MonsterModel );
                }
            } });

        //listen for a change to a monster location or state
        MonsterModel.setMonstersChangeListener(new Monsters.MonstersChangeListener() {
            @Override public void onMonstersChange(Monsters Monsters) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        //update the score
                        TextView tv_score = (TextView) findViewById(R.id.scoreValue);
                        int score = MonsterModel.getScore();
                        tv_score.setText( Integer.toString( score ) );

                        //redraw the board
                        MonsterView.invalidate();
                    }
                });
            } });

        //listen to a change in level
        MonsterModel.setLevelCompleteListener(new Monsters.LevelCompleteListener() {
            public void onLevelComplete(final Integer level) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //show a dialog. the message and option change based on if they beat the game
                        //or just beat the level
                        new AlertDialog.Builder(TouchMe.this)
                                .setTitle("Congratulations!")
                                .setMessage(
                                        ( level > Constants.HIGHEST_LVL ) ?
                                                "You beat the game! Restart at level 1?" :
                                                "You won! Would you like to try the next level?"
                                    )
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // next level
                                        clockModel.stop();
                                        MonsterModel.changeLevel(
                                                ( level > Constants.HIGHEST_LVL ) ? 1 : level
                                        );
                                        GridModel.acceptMonsters(MonsterModel);
                                        MonsterView.invalidate();

                                        clockModel.start();
                                    }
                                })
                                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing
                                        MonsterModel.killAllMonsters();

                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();

                        //update the level
                        TextView tv_level = (TextView) findViewById(R.id.levelValue);
                        tv_level.setText( Integer.toString( ( level > Constants.HIGHEST_LVL ) ? 1 : level  ) );
                        MonsterView.invalidate();
                    }
                });
            }
        });

        //set the controller to receive all the ticks. it will forward to the appropriate models
        clockModel.setOnTickListener( this );
        //start the clock
        clockModel.start();
    }

    /** Install an options menu. */
    @Override public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.simple_menu, menu);
        return true;
    }

    /** Respond to an options menu selection. */
    @Override public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_clear:
                resetToLevelOne();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /** Install a context menu. */
    @Override public void onCreateContextMenu(
        ContextMenu menu,
        View v,
        ContextMenuInfo menuInfo)
    {
        menu.add(Menu.NONE, 1, Menu.NONE, "Clear")
            .setAlphabeticShortcut('x');
    }

    /** Respond to a context menu selection. */
    @Override public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                resetToLevelOne();
                return true;
            default: ;
        }

        return false;
    }

    /**
     * receive an onTick from the clock model. forwards notifications to the other models
     * checks for a lose condition and prints a game over alert if met
     */
    public void onTick() {
        //delegate the event
        MonsterModel.onTick();
        final int time = MonsterModel.getTimeRemaining();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
            TextView tv_timeRemaining = (TextView) findViewById(R.id.timeRemainingValue);
            tv_timeRemaining.setText(Integer.toString(time));

            }
        });

        //stop the clock if timer is less than 1
        if (time < 1) {
            clockModel.stop();

            //check for lose condition
            if( MonsterModel.getSize() > 0) {
                //game over

                //print alert on the UI thread
                runOnUiThread( new Runnable() {
                    @Override
                    public void run() {
                        new AlertDialog.Builder(TouchMe.this)
                                .setTitle("Game Over")
                                .setMessage("You lost! Would you like to try again starting at level 1?")
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // reset game
                                        resetToLevelOne();
                                    }
                                })
                                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing
                                        MonsterModel.killAllMonsters();

                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                    }
                });
            }
        }

    }

    /**
     * resets the game as if they had just opened the app
     */
    public void resetToLevelOne() {

        //stop the clock, reset the level, kill all the monsters
        clockModel.stop();
        level = 1;
        MonsterModel.killAllMonsters();

        //update the view
        TextView tv_level = (TextView) findViewById(R.id.levelValue);
        tv_level.setText( "1" );
        MonsterView.invalidate();

        //change the level in the model
        MonsterModel.changeLevel( level );
        //tell the grid model to assign the new monsters cells
        GridModel.acceptMonsters( MonsterModel );
        //update the view
        MonsterView.invalidate();

        //start the clock
        clockModel.start();
    }

    /**
     * Preserves the model state for situations such device rotation.
     */
    @Override
    public void onSaveInstanceState(final Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        Log.i(TAG, "onSaveInstanceState");

    }

    /**
     * Restores the model state after situations such device rotation.
     */
    @Override
    public void onRestoreInstanceState(final Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Log.i(TAG, "onRestoreInstanceState");

    }
}