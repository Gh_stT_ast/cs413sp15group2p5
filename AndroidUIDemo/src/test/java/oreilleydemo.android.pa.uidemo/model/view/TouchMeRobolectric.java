package com.oreilly.demo.android.pa.uidemo;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;



@RunWith(RobolectricTestRunner.class)
public class TouchMeRobolectric extends AbstractTouchMeTest {

    private static String TAG = "TouchMe-android-activity-robolectric";

    private TouchMeActivity activity;

    @Before
    public void setUp() {
        activity = Robolectric.buildActivity(TouchMe.class).create().start().get();
    }

    @Override
    protected TouchMeActivity getActivity() {
        return activity;
    }
}
