package com.oreilleydemo.android.pa.uidemo.tests;

import android.test.ActivityInstrumentationTestCase2;
import android.test.UiThreadTest;

import com.oreilly.demo.android.pa.uidemo.AbstractTouchMeTest;
import com.oreilly.demo.android.pa.uidemo.TouchMe;

/**
 * Created by claudiusouca on 4/28/15.
 */
public class TouchMeTest extends ActivityInstrumentationTestCase2<TouchMe> {
    public TouchMeTest(Class<TouchMe> activityClass) {
        super(activityClass);
    }

    public TouchMeTest() {
        super(TouchMe.class);
        actualTest = new AbstractTouchMeTest() {
            @Override
            public TouchMe getActivity() {
                // return activity instance provided by instrumentation test
                return TouchMeTest.this.getActivity();
            }
        };
    }

    // test subclass instance to delegate to
    private AbstractTouchMeTest actualTest;

    // test that the Activity started properly
    public void testActivityTestCaseSetUpProperly() {
        actualTest.testActivityTestCaseSetUpProperly();
    }




    //Tests that the Values shown in View are the same stored as Integers
    @UiThreadTest
    public void testViewShowingValues() {
        actualTest.testActivity();
    }


    //Tests that state of Score,Time, and Level are maintained after screen rotation
    @UiThreadTest
    public void testActivityScenarioRotation() {
        actualTest.testActivityScenarioRotation();
    }


}
